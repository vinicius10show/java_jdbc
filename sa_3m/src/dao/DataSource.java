package dao;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataSource{

    private String hostname;
    private int port;
    private String database;
    private String username;
    private String password;
    private Connection con;
    private Statement st;
    private ResultSet rs;
    private String url = "https://bitbucket.org/Johnmartins/";

    private Connection connection;
    
    public DataSource(){
        try{
            hostname = "localhost";
            port     = 3306;
            database = "cruddb";  
            username = "root";
            password = "";
            
            String url = "jdbc:mysql://"+hostname+":"+port+"/"+database;
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            connection = (Connection) DriverManager.getConnection(url, username, password);
            System.out.println("Deu certo...");
        }
        catch(SQLException ex){
            System.err.println("Erro na conexão "+ex.getMessage());
        }
        catch(Exception ex){
            System.err.println("Erro geral "+ex.getMessage());
        }
    }
    
    public Connection getConnection(){
        return this.connection;
    }
    
    public void closeDataSource(){
        try{
            connection.close();
        }
        catch(Exception ex){
            System.err.println("Erro ao desconectar "+ex.getMessage());
        }
            
    }
    
  public void remover(int id) {
        try {

            String comando = "DELETE FROM clientes WHERE id=" + id;
            st = con.createStatement();
            st.executeUpdate(comando);
            System.out.println("Registro removido com sucesso");
        } catch (SQLException e) {
            System.out.println("Nao foi possivel remover");
        }
    }

    public void inserir(int id, String nome, String email, String telefone) {
        try {

            String comando = "INSERT INTO clientes (id, nome, email, telefone)VALUES(' " + id + " ',' " + nome + " ', " + email + ", " + telefone + " )";
            st = con.createStatement();
            st.executeUpdate(comando);
            st.close();
        } catch (SQLException e) {
            System.out.println("Erro ao cadastrar");
        }
    }

}